afs_lang = {}

-- ply:Kick() automatically adds "Disconnect:" before the kick message and "."
-- at the end of the kick message.

afs_lang.KickUnowned = "Please purchase a copy of Garry's Mod to play on this server"
afs_lang.KickLowHours = "You don't have the required playtime in Garry's Mod to join this server"
afs_lang.KickMinGames = "Your Steam account doesn't own enough games to join this server"
afs_lang.KickPrivateProfile = "This server requires a public Steam profile to join"
afs_lang.KickHiddenGames = "This server requires your Steam profile have 'Game Details' set to public"
afs_lang.KickHiddenPlaytime =
    "This server requires your Steam profile have 'Game Details > Total Playtime' set to public"
afs_lang.KickNoProfile = "Please set up your Steam profile before you join this server"
