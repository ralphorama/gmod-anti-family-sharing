--[[
    AntiFamilySharing: Filter players who don't own Garry's Mod from your server.
    Copyright (C) 2023  Ralph Drake

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
--]]


CreateConVar("afs_sv_apikey", "",
    FCVAR_PROTECTED + FCVAR_UNLOGGED + FCVAR_DONTRECORD + FCVAR_PRINTABLEONLY,
    "The Steam Web API key for AntiFamilySharing."
)

CreateConVar("afs_sv_kick_noprofile", 1, FCVAR_NONE,
    "Should AntiFamilySharing kick players who haven't set up their Steam profile?",
    0, 1
)
CreateConVar("afs_sv_kick_privateprofile", 1, FCVAR_NONE,
    "Should AntiFamilySharing kick players with a private Steam profile?",
    0, 1
)

CreateConVar("afs_sv_kick_mingames", 0, FCVAR_NONE,
    "Should AntiFamilySharing kick players who own fewer than afs_sv_mingames?",
    0, 1
)
CreateConVar("afs_sv_mingames", 2, FCVAR_NONE,
    "Minimum games a player must own to join the server if afs_sv_kick_mingames is enabled.",
    0
)

CreateConVar("afs_sv_kick_minplaytime", 0, FCVAR_NONE,
    "Should AntiFamilySharing kick players who haven't played enough Garry's Mod?",
    0, 1
)
CreateConVar("afs_sv_minplaytimehours", 2, FCVAR_NONE,
    "Minimum hours of Garry's Mod playtime a user must have if afs_sv_kick_minplaytime is enabled.",
    0
)

CreateConVar("afs_sv_enabled", 1, FCVAR_NONE,
    "Enables/disables the entire AntiFamilySharing addon. Set to 0 as a temporary fix for issues with this addon.",
    0, 1
)
CreateConVar("afs_sv_debug", 0, FCVAR_NONE,
    "Enable/disable AntiFamilySharing debug output to the server console.",
    0, 1
)

local AntiFamilySharing = {}

AntiFamilySharing.APIKey = GetConVar("afs_sv_apikey")

AntiFamilySharing.KickNoProfile = GetConVar("afs_sv_kick_noprofile")

AntiFamilySharing.KickPrivateProfiles = GetConVar("afs_sv_kick_privateprofile")

AntiFamilySharing.EnforceMinGames = GetConVar("afs_sv_kick_mingames")
AntiFamilySharing.MinGameCount = GetConVar("afs_sv_mingames")

AntiFamilySharing.EnforceMinPlaytime = GetConVar("afs_sv_kick_minplaytime")
AntiFamilySharing.MinPlaytimeHours = GetConVar("afs_sv_minplaytimehours")

AntiFamilySharing.Enabled = GetConVar("afs_sv_enabled")
AntiFamilySharing.Debug = GetConVar("afs_sv_debug")

include("antifamilysharing/server/lang.lua")
AntiFamilySharing.lang = afs_lang or {}

--------------------------------------------------------------------------------
--  Don't touch anything below this line unless you know what you're doing!   --
--  (Note to devs: this code flows bottom-up and uses a lot of callbacks.)    --
--------------------------------------------------------------------------------

AntiFamilySharing.consts = {}

AntiFamilySharing.consts.SemVer = "v3.1.1"
AntiFamilySharing.consts.APIBaseURL = "https://api.steampowered.com"

--[[
    Prints a message to server console that we can easily find in the logs.

    @param message  any string
--]]
local function DebugPrint(message)
    if AntiFamilySharing.Debug:GetBool() then
        print(string.format("[AntiFamilySharing] DEBUG | %s", message))
    end
end

--[[
    Checks if ULib (ULX) is installed, kicks using ULib.kick if it is.

    @param ply      Player to kick
    @param message  Kick message sent to player/logged in console
--]]
local function KickPlayer(ply, reason)
    if ULib and ULib.kick then
        DebugPrint(string.format(
            "KickPlayer: Kicking \"%s\" (%s) using ULib.kick()",
            ply:Nick(),
            ply:SteamID64()
        ))

        ULib.kick(ply, reason)
    else
        DebugPrint(string.format(
            "KickPlayer: Kicking \"%s\" (%s) using ply:Kick()",
            ply:Nick(),
            ply:SteamID64()
        ))

        ply:Kick(reason)
    end
end

--[[
    Wrapper for the Steam Web API IPlayerService/GetOwnedGames endpoint.

    @param ply      glua Player object
    @param onlyGmod whether to filter response to only Garry's Mod (AppID 4000)
    @param callback function to call on successful request, receives a table
                    with response data.
--]]
local function GetOwnedGames(ply, onlyGmod, callback)
    local appidFilter = ""
    if onlyGmod then
        appidFilter = "&appids_filter[0]=4000"
    end

    http.Fetch(
        string.format(
            "%s/IPlayerService/GetOwnedGames/v0001/?key=%s&steamid=%s&format=json%s",
            AntiFamilySharing.consts.APIBaseURL,
            AntiFamilySharing.APIKey:GetString(),
            ply:SteamID64(),
            appidFilter
        ),
        function (body, _, _, statusCode)
            -- This function may still be called for codes other than 200, which
            -- is out of the ordinary for the Steam Web API.
            if statusCode ~= 200 then
                error(string.format(
                    "[AntiFamilySharing] Invalid HTTP response code %s when fetching owned games for \"%s\" (%s)\n",
                    statusCode, ply:Nick(), ply:SteamID64()
                ))
            end

            local bodyTbl = util.JSONToTable(body)

            -- TODO: make sure this works if bodyTbl/bodyTbl.response is {}
            if istable(bodyTbl) and istable(bodyTbl.response) then
                callback(bodyTbl.response)
            else
                error(string.format(
                    "GetOwnedGames: Error parsing response data from the Steam API: %s\n",
                    body
                ))
            end
        end,
        function (errorCode)
            error(string.format(
                "[AntiFamilySharing] Failed to fetch owned games list for \"%s\" (%s) (Error: %s)\n",
                ply:Nick(), ply:SteamID64(), errorCode
            ))
        end
    )
end

--[[
    Wrapper for the Steam Web API ISteamUser/GetPlayerSummaries endpoint.
    Requests profile data for the player passed in the ply param.

    @param ply      glua Player object
    @param callback function to call on successful request, receives a table
                    with player profile data.
--]]
local function GetPlayerSummaries(ply, callback)
    http.Fetch(
        string.format(
            "%s/ISteamUser/GetPlayerSummaries/v0002/?key=%s&steamids=%s&format=json",
            AntiFamilySharing.consts.APIBaseURL,
            AntiFamilySharing.APIKey:GetString(),
            ply:SteamID64()
        ),
        function (body, _, _, statusCode)
            -- This function may still be called for codes other than 200, which
            -- is out of the ordinary for the Steam Web API.
            if statusCode ~= 200 then
                error(string.format(
                    "[AntiFamilySharing] Invalid HTTP response code %s when fetching Steam profile for \"%s\" (%s)\n",
                    statusCode, ply:Nick(), ply:SteamID64()
                ))
            end

            local bodyTbl = util.JSONToTable(body)

            if istable(bodyTbl) and istable(bodyTbl.response) and istable(bodyTbl.response.players[1]) then
                callback(bodyTbl.response.players[1])
            else
                error(string.format(
                    "GetPlayerSummaries: Error parsing response data from the Steam API: %s\n",
                    body
                ))
            end
        end,
        function (errorCode)
            error(string.format(
                "[AntiFamilySharing] Failed to fetch Steam profile data for \"%s\" (%s) (Error: %s)\n",
                ply:Nick(), ply:SteamID64(), errorCode
            ))
        end
    )
end

--[[
    Calls GetOwnedGames with a check for *all* games owned and parses the
    callback response to see if Player ply meets the owned games requirement to
    join the server.

    @param ply  glua Player object
--]]
local function CheckOwnedGamesCount(ply)
    -- Step 4 (optional): Check if a player owns the required minimum number
    -- of games.
    DebugPrint(string.format(
        "CheckOwnedGamesCount: Checking how many games \"%s\" (%s) owns.",
        ply:Nick(), ply:SteamID64()
    ))

    GetOwnedGames(ply, false, function (res)
        -- This function is called when we already know the player owns at least
        -- one game (Garry's Mod), so we know their profile is public as well as
        -- their game list.
        DebugPrint(string.format(
            "CheckOwnedGamesCount: Player \"%s\" (%s) owns %s games.",
            ply:Nick(), ply:SteamID64(), res.game_count
        ))

        if res.game_count < AntiFamilySharing.MinGameCount:GetInt() then
            KickPlayer(ply, AntiFamilySharing.lang.KickMinGames)
            return
        end

        DebugPrint(string.format(
            "CheckOwnedGamesCount: Player \"%s\" (%s) Passed all enabled checks, allowing them to join the server!",
            ply:Nick(), ply:SteamID64()
        ))
    end)
end

--[[
    Calls GetPlayerSummaries and parses the output to determine if Player ply's
    profile is configured and public.

    @param ply  glua Player object
--]]
local function CheckUserProfile(ply)
    -- Step 2: Check profile availability
    DebugPrint(string.format(
        "CheckUserProfile: Checking if \"%s\" (%s) has a public/configured profile.",
        ply:Nick(), ply:SteamID64()
    ))

    GetPlayerSummaries(ply, function (profileData)
        -- Users with profiles that "aren't set up yet" will have timecreated,
        -- but won't have profilestate == 1
        if not profileData.profilestate then
            if AntiFamilySharing.KickNoProfile:GetBool() then
                KickPlayer(ply, AntiFamilySharing.lang.KickNoProfile)
            else
                -- We can't run the rest of the checks on an unconfigured Steam
                -- profile, so just bail out here.
                DebugPrint(string.format(
                    "CheckUserProfile: Player \"%s\" (%s) doesn't have a Steam profile, cancelling further checks.",
                    ply:Nick(), ply:SteamID64()
                ))
            end
        elseif not profileData.timecreated then
            if AntiFamilySharing.KickPrivateProfiles:GetBool() then
                KickPlayer(ply, AntiFamilySharing.lang.KickPrivateProfile)
            else
                -- We can't run the rest of the checks on a private Steam
                -- profile, so just bail out here.
                DebugPrint(string.format(
                    "CheckUserProfile: Player \"%s\" (%s) has a private Steam profile, cancelling further checks.",
                    ply:Nick(), ply:SteamID64()
                ))
            end
        else
            -- At this point we know their profile is public but their game list
            -- is hidden.
            KickPlayer(ply, AntiFamilySharing.lang.KickHiddenGames)
        end
    end)
end

--[[
    Calls GetOwnedGames with only a check for Garry's Mod ownership and parses
    the response from the callback to check if Player ply owns Garry's Mod and,
    optionally, meets the playtime requirement to join the server.

    @param ply  glua Player object
--]]
local function CheckGmodOwnership(ply)
    -- We theoretically should never hit this codepath since AFSInit() removes
    -- the hook that calls this function when the addon is disabled. Better
    -- safe than sorry, I suppose.
    if not AntiFamilySharing.Enabled:GetBool() then
        DebugPrint("CheckGmodOwnership: Addon is disabled, bailing out!")
        return
    end

    -- Ignore bots
    if ply:IsBot() then return end

    -- Step 1: Check if the user owns Garry's Mod
    DebugPrint(string.format(
        "CheckGmodOwnership: Checking if \"%s\" (%s) owns Garry's Mod.",
        ply:Nick(), ply:SteamID64()
    ))

    -- Easiest possible check so we can skip extra logic
    if ply:SteamID64() ~= ply:OwnerSteamID64() then
        KickPlayer(ply, AntiFamilySharing.lang.KickUnowned)
        return
    end

    GetOwnedGames(ply, true, function (res)
        if not res.game_count then
            -- if game_count is nil then we need to check profile status
            CheckUserProfile(ply)
            return
        elseif res.game_count == 0 then
            -- game_count will be 0 if the player has a public game inventory
            -- but doesn't own Garry's Mod.
            KickPlayer(ply, AntiFamilySharing.lang.KickUnowned)
            return
        end

        -- Now we know res.game_count == 1, so check playtime first and number
        -- of games owned second

        DebugPrint(string.format(
            "CheckGmodOwnership: Player \"%s\" (%s) owns Garry's Mod!",
            ply:Nick(), ply:SteamID64()
        ))

        if AntiFamilySharing.EnforceMinPlaytime:GetBool() then
            DebugPrint(string.format(
                "CheckGmodOwnership: Checking if \"%s\" (%s) meets playtime requirements.",
                ply:Nick(), ply:SteamID64()
            ))

            -- playtime_forever will be 0 if total playtime is hidden
            -- to hit this case otherwise, a player would have to have joined
            -- this server immediately after buying Garry's Mod
            if res.games[1].playtime_forever == 0 then
                KickPlayer(ply, AntiFamilySharing.lang.KickHiddenPlaytime)
                return
            end

            -- We round this to 2 places so it looks nicer in debug messages.
            local hoursPlayed = math.Round(res.games[1].playtime_forever / 60, 2)

            DebugPrint(string.format(
                "CheckGmodOwnership: \"%s\" (%s) has played %s hours of Garry's Mod",
                ply:Nick(), ply:SteamID64(), hoursPlayed
            ))

            if hoursPlayed < AntiFamilySharing.MinPlaytimeHours:GetInt() then
                KickPlayer(ply, AntiFamilySharing.lang.KickLowHours)
                return
            end
        end

        if AntiFamilySharing.EnforceMinGames:GetBool() then
            -- At this point we know the player owns Garry's Mod, and, if
            -- needed, meets playtime requirements.
            -- We need to check they own enough games to join the server here.
            CheckOwnedGamesCount(ply)
            return
        end

        DebugPrint(string.format(
            "CheckGmodOwnership: Player \"%s\" (%s) Passed all enabled checks, allowing them to join the server!",
            ply:Nick(), ply:SteamID64()
        ))
    end)
end

--[[
    Check API Key validity and add hook for PlayerAuthed logic.
--]]
local function AFSInit()
    if not AntiFamilySharing.Enabled:GetBool() then
        local hookTable = hook.GetTable()

        if istable(hookTable) and hookTable.PlayerAuthed.AntiFamilySharing then
            DebugPrint("AFSInit: Removing PlayerAuthed hook for CheckGmodOwnership.")
            hook.Remove("PlayerAuthed", "AntiFamilySharing")
        end

        print("[AntiFamilySharing] Addon is disabled! Run afs_sv_enabled 1 to enable the addon.")
        return
    end

    if string.len(AntiFamilySharing.APIKey:GetString()) ~= 32 or
       AntiFamilySharing.APIKey:GetString() == "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    then
        error("Your API key is improperly configured. Please set afs_sv_apikey in console or server.cfg")
    end

    -- Call step 1 of our logic on PlayerAuthed and let the callbacks flow like wine
    hook.Add("PlayerAuthed", "AntiFamilySharing", CheckGmodOwnership)

    print(string.format(
        "[AntiFamilySharing] Version %s enabled! Waiting for players to connect...",
        AntiFamilySharing.consts.SemVer
    ))
end

-- AddChangeCallback is automatically called when afs_sv_apikey is set by
-- server.cfg executing, at which point afs_sv_apikey should be set to a valid
-- Steam Web API key.
hook.Add("Initialize", "AntiFamilySharingPreInit", function()
    cvars.AddChangeCallback("afs_sv_apikey", function() AFSInit() end, "AFSAPIKeyChangeCallback")
    cvars.AddChangeCallback("afs_sv_enabled", function() AFSInit() end, "AFSAPIKeyChangeCallback")

    print(string.format(
        "[AntiFamilySharing] Version %s loaded, waiting for API key from server.cfg...",
        AntiFamilySharing.consts.SemVer
    ))
end)

concommand.Add("afs_sv_version", function()
    print(string.format(
        "[AntiFamilySharing] Running version %s. The addon is currently %s.",
        AntiFamilySharing.consts.SemVer,
        AntiFamilySharing.Enabled:GetBool() and "ENABLED" or "DISABLED"
    ))
end, nil, "Prints the version of AntiFamilySharing your server is running.")
