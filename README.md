# AntiFamilySharing for Garry's Mod

This addon is a simple solution for preventing users who don't outright own
Garry's mod from joining your server. It is relatively uncomplicated and
easy to install/set up.

AntiFamilySharing automatically kicks players who don't own a copy of Garry's
Mod. Additionally, players who have their game list set to "Private" will also
be kicked, since AFS cannot check if they own Garry's Mod in that case.

AFS also, by default, kicks players with unconfigured/private profiles. You can
find ways to disable this behavior [on the Wiki][8]. You can also configure AFS
to kick players who haven't played enough hours of Garry's Mod or don't own
enough games on Steam. More info about when and why AFS kicks players
[is available here][9].

There is no option to disable kicking of family shared accounts. If you don't
want them kicked, don't install this addon.


## Installation

1. Register a Steam Web API key [here][1].
   - You can put whatever you want for "domain," it doesn't matter.
2. Add this addon to your server's workshop collection.
   - See [Workshop for Dedicated Servers][7] if you need help with this step.
3. Follow the steps in __Configuration__ below.
4. Restart your server to install the addon.


## Configuration

### Required Configuration Option: Steam Web API Key

**You must add the following line to `garrysmod/cfg/server.cfg`!**

```
// Replace XXXXX with your Steam Web API Key from step 1.
// You can find your API Key here: https://steamcommunity.com/dev/apikey
afs_sv_apikey "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

### Advanced Configuration Settings

For information about advanced addon configuration, please [click here][8] to
visit the Advanced Configuration wiki page.

### Language Configuration

If you would like to change the messages AFS sends to players when they are
kicked, please [click here][10] to visit the Language Customization wiki page.


## Methodology

I created this addon because existing solutions were all based on the
deprecated `IPlayerService/IsPlayingSharedGame` Steam API endpoint, which no
longer returns any data.

This addon instead uses the [`ISteamUser/GetPlayerSummaries`][3],
[`IPlayerService/GetRecentlyPlayedGames`][5], and
[`IPlayerService/GetOwnedGames`][4] APIs which are all well-established and
will hopefully not be deprecated in the same way.

### Security

Keeping the Steam Web API key secure is, of course, a top priority for this
addon. The following measures are in place to keep the API Key secure:

1. The convar that stores the API key, `afs_sv_apikey` is created with FCVAR_PROTECTED, FCVAR_UNLOGGED, FCVAR_DONTRECORD, and FCVAR_PRINTABLEONLY flags.
   - This should ensure the API key is only ever permanently stored in `server.cfg`
   - The contents are not sent back to the client if a command like `ulx rcon` is used.
2. All of the code for this addon is exclusively server-side.

With these steps taken, the only two ways an attacker could steal an API key
from this addon are either gaining access to the server console or gaining
access to the server filesystem.

If you are worried about attackers stealing values stored in `server.cfg` (past
exploits like the ["Cough Virus"][11] did this), you can set `sv_allowupload`
to `0`. Again, if a hacker gains access to your server's filesystem, you have a
much bigger problem on your hands.


## Bugs and Pull Requests

If you are experiencing issues with the addon and want to temporarily disable it
without restarting your server, you can set `afs_sv_enabled` to `0`.

You can report any bugs you encounter [here][6]. Please make sure to submit logs
with `afs_sv_debug` set to `1` when submitting a bug report.

For pull requests, please make sure your code passes [glualint][2] with the
settings specified in [`.glualint.json](.glualint.json). Please test your
changes thoroughly using a separate account that is not friends with the account
providing the Steam API key. (Also, make sure not to commit your API key, lol.)


## License

```
AntiFamilySharing: Filter players who don't own Garry's Mod from your server.
Copyright (C) 2023  Ralph Drake

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
```


[1]: https://steamcommunity.com/dev/apikey
[2]: https://github.com/FPtje/GLuaFixer
[3]: https://developer.valvesoftware.com/wiki/Steam_Web_API#GetPlayerSummaries_(v0002)
[4]: https://developer.valvesoftware.com/wiki/Steam_Web_API#GetOwnedGames_(v0001)
[5]: https://developer.valvesoftware.com/wiki/Steam_Web_API#GetRecentlyPlayedGames_(v0001)
[6]: https://gitlab.com/ralphorama/gmod-anti-family-sharing/-/issues/new
[7]: https://wiki.facepunch.com/gmod/Workshop_for_Dedicated_Servers
[8]: https://gitlab.com/ralphorama/gmod-anti-family-sharing/-/wikis/home/Configuration/Advanced-Configuration
[9]: https://gitlab.com/ralphorama/gmod-anti-family-sharing/-/wikis/home/Kick-Logic
[10]: https://gitlab.com/ralphorama/gmod-anti-family-sharing/-/wikis/home/Configuration/Language-Customization
[11]: https://www.pcgamer.com/garrys-mod-cough-virus-is-cured-but-it-could-have-been-worse/
