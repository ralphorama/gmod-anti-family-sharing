<!--
    Before submitting a PR, please read CONTRIBUTING.md in the root of this repo.

    Please note that if your code doesn't pass the `glualint` CI check it will
    not be merged.
-->

## Summary

<!-- Please write a brief summary of what your PR adds/fixes/etc. -->


## PR Checklist

<!--
    Please complete each of these items and change each box from [ ] to [x].
-->

- [ ] I linted my code with `glualint`
- [ ] I ensured `afs_sv_apikey` is **never** shared with the client.
    - [ ] I did not network the Steam Web API key to the client **at all**
- I tested my code...
    - [ ] On a server
    - [ ] With `afs_sv_debug` enabled
    - [ ] And did not encounter errors
