<!--
    IMPORTANT: Please follow these steps when filing a bug report:

    - If you are reporting Lua errors, please copy the errors and paste them
    into the "Debug Output / Error Logs" section at the bottom of this template.

    - If you are reporting issues with player kicking, please:
       1. Run afs_sv_debug "1" in your server console
       2. Copy lines that start with "[AntiFamilySharing] DEBUG |"
       3. Paste them into the "Debug Output / Error Logs" section below.
-->

## Summary

<!-- Please describe the issue you are having here. -->


## Checklist

- [ ] I have ensured `afs_sv_apikey` is properly configured
- [ ] I checked the version of the addon with `afs_sv_version`
    - I am running version __<!-- paste the output of afs_sv_version here -->__


## Current config options

<!--
    Please pecify your current config options here.

    DO __NOT__ ADD YOUR STEAM API KEY!
-->

```c
// Current AFS configuration options.
// You can paste these commands into console (without the "") to check what
// their current values are set to.

afs_sv_enabled ""

afs_sv_kick_noprofile ""
afs_sv_kick_privateprofile ""

afs_sv_kick_mingames ""
afs_sv_mingames ""

afs_sv_kick_minplaytime ""
afs_sv_minplaytimehours ""

afs_sv_debug ""
```


## Debug Output / Error Logs

```
<!-- Please paste debug logs and/or lua errors from console here -->
```
