#!/bin/bash

set -e

GLUALINT_PATH=".cache/glualint"

if [ ! -d .cache ]; then
    echo "Creating cache directory..."
    mkdir -p ".cache"
else
    echo "Cache directory already exists."
fi

if [ ! -f "$GLUALINT_PATH" ]; then
    echo "Downloading glualint v$GLUALINT_VERSION..."

    curl -C - -sSL -o .cache/glualint-$GLUALINT_VERSION-x86_64-linux.zip \
        "https://github.com/FPtje/GLuaFixer/releases/download/$GLUALINT_VERSION/glualint-$GLUALINT_VERSION-x86_64-linux.zip"

    unzip -d ".cache/" ".cache/glualint-$GLUALINT_VERSION-x86_64-linux.zip"
    chmod +x "$GLUALINT_PATH"

    echo "glualint v`./.cache/glualint version` installed."
else
    echo "glualint v`./.cache/glualint version` already installed."
fi

echo "Linting with glualint..."

./.cache/glualint --config "./.glualint.json" --output-format github lint "lua/" && \
    echo "Linting succeded with no issues!"
