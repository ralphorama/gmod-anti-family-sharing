# Contributing to AntiFamilySharing

## Security

Since this addon deals with important secrets (the Steam Web API key), please
ensure the following requirements are met:

1. The contents of `afs_sv_apikey` are never shared with the client.
2. The contents of `afs_sv_apikey` are never saved in log files.
3. The API key is never printed to console.

For the sake of ensuring security, you should not write code that executes on
the client in any capacity. I will close your PR because I do not want to verify
client-side code is secure.


## Code Style

This project follows a few rules for code style:

1. Lines must not exceed 120 characters.
2. Scope depth (i.e. nested `if`, `for`, etc. statements) must not exceed 7 levels.
3. Early `return`s are preferred to nested `if` statements.
4. Calls to the Steam Web API should be minimized.

### Linting

This project uses a custom [`.glualint.json`][.glualint.json] config file,
please ensure your installation of `glualint` is linting according to the rules
in this file.

All PRs are automatically linted using `glualint` via CI pipeline. If your PR
doesn't pass this pipeline, it will not be merged under any circumstances.

### Commit Messages

By default, PRs are squash merged to the `master` branch, largely eliminating
the contents of contained commit messages. I myself like to loosely follow the
[commit message format][1] used by AngularJS. You can simplify it to:

```
<type>: <short summary>

<any additional notes>

[closes/fixes #<issue number>]
```

Check [the repo's commit history][2] for more examples. While I won't ask you
to strictly adhere to this format, please avoid commit messages like "stuff" or
"fix" etc. It just looks bad lol.


[1]: https://github.com/angular/angular/blob/main/CONTRIBUTING.md#commit
[2]: https://gitlab.com/ralphorama/gmod-anti-family-sharing/-/commits/master?ref_type=heads
